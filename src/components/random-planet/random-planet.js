import React, { Component } from 'react';
import withService from '../hoc/withService';
import CircularProgress from '@material-ui/core/CircularProgress';

import './random-planet.css';


class RandomPlanet extends Component {

  generateRandomNumber = () => {
    let result = null
    if (this.props.property !== null) {
      result = Math.floor(Math.random() * this.props.property.length);
      console.log(result);
    }
    return result;
  }

  render() {
    let randomNumber = this.generateRandomNumber();
    let info = null;
    if (this.props.property != null) {
      info = this.props.property[randomNumber];
    }
    let planetImageLink = `https://starwars-visualguide.com/assets/img/planets/${randomNumber}.jpg`
    return info === null ? <CircularProgress /> :
      (
        <div className="random-planet jumbotron rounded">
          <img className="planet-image"
            src={planetImageLink} />
          <div>
            <h4>{info.name}</h4>
            <ul className="list-group list-group-flush">
              <li className="list-group-item">
                <span className="term">Population</span>
                <span>{info.population}</span>
              </li>
              <li className="list-group-item">
                <span className="term">Rotation Period</span>
                <span>{info.rotation_period}</span>
              </li>
              <li className="list-group-item">
                <span className="term">Diameter</span>
                <span>{info.diameter}</span>
              </li>
            </ul>
          </div>
        </div>

      );
  }

}

const RandomPlanetWithService = withService(RandomPlanet);

export default RandomPlanetWithService;
