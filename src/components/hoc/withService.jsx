import React, { useEffect, useState } from 'react';

export default function withService(Comp) {
    return (props) => {
        const [property, setProperty] = useState(null)
        useEffect(() => {
            props.request().then((data) => {
                setProperty(data.results);
            })
        }, []);
        return <Comp property={property} {...props} />
    }
}


