import React, { useContext, useState, useEffect } from 'react';
import { NameContext } from '../app/app';

import Service from '../../services/service';
import CircularProgress from '@material-ui/core/CircularProgress';


import './person-details.css';

const PersonDetails = () => {
  const [information, setInformation] = useState(null)
  const [image, setImage] = useState(null)

  const { profileInformation } = useContext(NameContext);

  useEffect(() => {
    setInformation(profileInformation)

    if (profileInformation !== null) {
      const symbol = profileInformation.url.match(/\d+/)
      const type = profileInformation.url.split('/')

      new Service()
        .getImage(symbol, type[4])
        .then((url) => setImage(url))
    }
    return(() => {setImage(null)})
  }, [profileInformation])

  if (information) {
    return (
      <div className="person-details card">
        {image === null ? <CircularProgress />
          : <img className="person-image"
            src={image} />}

        <div className="card-body">
          <h4>{information.name}</h4>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              {information.gender ?
                <div>
                  <span className="term">Gender</span>
                  <span>{information.gender}</span>
                </div>
                : information.diameter ?
                  <div>
                    <span className="term">Diameter</span>
                    <span>{information.diameter}</span>
                  </div>
                  :
                  <div>
                    <span className="term">Crew</span>
                    <span>{information.crew}</span>
                  </div>
              }
            </li>
            <li className="list-group-item">
              {information.birth_year ?
                <div>
                  <span className="term">Birth Year</span>
                  <span>{information.birth_year}</span>
                </div>
                : information.orbital_period ?
                  <div>
                    <span className="term">Orbital period</span>
                    <span>{information.orbital_period}</span>
                  </div>
                  :
                  <div>
                    <span className="term">Length</span>
                    <span>{information.length}</span>
                  </div>
              }
            </li>
            <li className="list-group-item">
              {information.eye_color ?
                <div>
                  <span className="term">Eye color</span>
                  <span>{information.eye_color}</span>
                </div>
                : information.population ?
                  <div>
                    <span className="term">Population</span>
                    <span>{information.population}</span>
                  </div>
                  :
                  <div>
                    <span className="term">Model</span>
                    <span>{information.model}</span>
                  </div>
              }
            </li>
          </ul>
        </div>
      </div>
    )
  } else {
    return (<div></div>)
  }
}

export default PersonDetails;