import React, { createContext, useState, useEffect } from 'react';

import Header from '../header';
import ItemListWithService from '../item-list/item-list';
import RandomPlanetWithService from '../random-planet/random-planet';
import PersonDetails from '../person-details';
import Service from '../../services/service';
import Error_boundary from '../../components/error_boundary/error_boundary';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";


import './app.css';


const NameContext = createContext();
const PersonContext = createContext();

const App = () => {

  const [profileInformation, setProfileInformation] = useState(null);

  useEffect(() => {
  })

  const onElementInfo = (value) => {
    setProfileInformation(value);

  }

  return (
    <Error_boundary>
      <Router>
        <NameContext.Provider value={{ onElementInfo: onElementInfo, profileInformation: profileInformation }}>
          <Header />
          <RandomPlanetWithService request={() => new Service().getPlanets()}/>
          <div className="row mb2">
            <Route exact path='/people'>
              <div className="col-md-6">
                <ItemListWithService request={() => new Service().getPeoples()} />
              </div>
            </Route>
            <Route exact path='/planets'>
              <div className="col-md-6">
                <ItemListWithService request={() => new Service().getPlanets()} />
              </div>
            </Route>
            <Route exact path='/starships'>
              <div className="col-md-6">
                <ItemListWithService request={() => new Service().getStarships()} />
              </div>
            </Route>
            <div className="col-md-6">
              <PersonContext.Provider value={profileInformation}>
                <PersonDetails />
              </PersonContext.Provider>
            </div>
          </div>
        </NameContext.Provider>
      </Router>
    </Error_boundary>
  );
};

export { NameContext, PersonContext };
export default App;